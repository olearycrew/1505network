#!/bin/bash

echo "Whitelisting"

echo "My custom whitelist"
pihole -w brendanoleary.com
pihole -w wbal.com
pihole -w wbaltv.com
pihole -w analytics.google.com
pihole -w liscioapps.com
pihole -w shopapps.liscioapps.com
pihole -w addtoany.com
pihole -w api.bufferapp.com
pihole -w analytics.twitter.com
pihole -w platform.twitter.com
pihole -w google-analytics.com
pihole -w metrics.api.heroku.com
pihole -w v2.zopim.com
pihole -w mediator.zopim.com
pihole -w files.slack.com

echo "Paramount Plus sucks"
pihole -w cbsi.com
pihole --white-regex "(.|)cbsi.com$"
pihole -w cbsinteractive.hb.omtrdc.net
pihole --white-regex "(.|)cws.conviva.com$"
pihole -w pubads.g.doubleclick.net
pihole -w cbsi.com.ssl.sc.omtrdc.net
pihole -w cbsinteractive.hb.omtrdc.net
pihole -w saa.cbsi.com

echo "NBC Sports is the worst"
pihole -w nbcsports.com
pihole -w geo.nbcsports.com
pihole -w nbcu.demdex.net
pihole -w nbcume.sc.omtrdc.net
pihole -w dpm.demdex.net

echo "Enable snowplow - sorry future self"
pihole -w d1fc8wv8zag5ca.cloudfront.net

echo "Whitelist from https://discourse.pi-hole.net/t/commonly-whitelisted-domains/212"
pihole -w clients4.google.com 
pihole -w clients2.google.com
pihole -w s.youtube.com 
pihole -w video-stats.l.google.com
pihole -w android.clients.google.com
pihole -w www.msftncsi.com
pihole -w outlook.office365.com products.office.com c.s-microsoft.com i.s-microsoft.com login.live.com
pihole -w g.live.com
pihole -w dl.delivery.mp.microsoft.com geo-prod.do.dsp.mp.microsoft.com displaycatalog.mp.microsoft.com
pihole -w clientconfig.passport.net 
pihole -w v10.events.data.microsoft.com
pihole -w xbox.ipv6.microsoft.com device.auth.xboxlive.com www.msftncsi.com title.mgt.xboxlive.com xsts.auth.xboxlive.com title.auth.xboxlive.com ctldl.windowsupdate.com attestation.xboxlive.com xboxexperiencesprod.experimentation.xboxlive.com xflight.xboxlive.com cert.mgt.xboxlive.com xkms.xboxlive.com def-vef.xboxlive.com notify.xboxlive.com help.ui.xboxlive.com licensing.xboxlive.com eds.xboxlive.com www.xboxlive.com v10.vortex-win.data.microsoft.com settings-win.data.microsoft.com
pihole -w s.gateway.messenger.live.com ui.skype.com pricelist.skype.com apps.skype.com m.hotmail.com s.gateway.messenger.live.com sa.symcb.com s{1..5}.symcb.com 
pihole -w officeclient.microsoft.com
pihole -w spclient.wg.spotify.com apresolve.spotify.com
pihole -w plex.tv tvdb2.plex.tv pubsub.plex.bz proxy.plex.bz proxy02.pop.ord.plex.bz cpms.spop10.ams.plex.bz meta-db-worker02.pop.ric.plex.bz meta.plex.bz tvthemes.plexapp.com.cdn.cloudflare.net tvthemes.plexapp.com 106c06cd218b007d-b1e8a1331f68446599e96a4b46a050f5.ams.plex.services meta.plex.tv cpms35.spop10.ams.plex.bz proxy.plex.tv metrics.plex.tv pubsub.plex.tv status.plex.tv www.plex.tv node.plexapp.com nine.plugins.plexapp.com staging.plex.tv app.plex.tv o1.email.plex.tv  o2.sg0.plex.tv dashboard.plex.tv
pihole -w gravatar.com
pihole -w thetvdb.com 
pihole -w themoviedb.com 
pihole -w placehold.it placeholdit.imgix.net
pihole -w dl.dropboxusercontent.com ns1.dropbox.com ns2.dropbox.com
pihole -w itunes.apple.com
pihole -w imagesak.secureserver.net
pihole -w dl.google.com
pihole -w appleid.apple.com
#pihole -w app-analytics.snapchat.com sc-analytics.appspot.com cf-st.sc-cdn.net
#pihole -w fpdownload.adobe.com entitlement.auth.adobe.com livepassdl.conviva.com
pihole -w gfwsl.geforce.com
#pihole -w delivery.vidible.tv img.vidible.tv videos.vidible.tv edge.api.brightcove.com cdn.vidible.tv
#pihole -w dev.virtualearth.net ecn.dev.virtualearth.net t0.ssl.ak.dynamic.tiles.virtualearth.net t0.ssl.ak.tiles.virtualearth.net
pihole -w android.clients.google.com
#pihole -w appspot-preview.l.google.com
pihole -w connectivitycheck.android.com android.clients.google.com clients3.google.com  connectivitycheck.gstatic.com 
pihole -w msftncsi.com www.msftncsi.com ipv6.msftncsi.com
pihole -w captive.apple.com gsp1.apple.com www.apple.com www.appleiphonecell.com

