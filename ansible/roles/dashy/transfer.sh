#!/bin/bash

if [ -n "$1" ]
then
    if [ $1 == "download" ]
    then
        echo "Downloading from server-01"
        scp brendan@server-01.local:/opt/dashy/dashy-config.yml ./
        echo "Done"
    elif [ $1 == "upload" ]
    then
        echo "Uploading to server-01"
        scp ./dashy-config.yml brendan@server-01.local:/opt/dashy
        echo "Done"
    else
        echo "Please specify download or upload"
    fi
else
    echo "Please specify download or upload"
fi