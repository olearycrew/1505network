# Ansible

## Getting Started
* `brew install ansible` OR `pip3 install --user ansible`
* `ansible-galaxy install -r requirements.yml`
* `ansible-playbook -i inventory playbook.yml --ask-vault-pass`

## Hosts
* Ensure SSH key in `~/.ssh/authorized_keys`
* Ensure `{username} ALL=(ALL) NOPASSWD:ALL` in sudoers file (with `visudo`)
* Install python3 if needed `sudo apt-get install python3.6`

### Pihole Hosts
If you're going to run Pihole or another DNS on the host, you need to disable the DNS listener:

* `sudo vim /etc/systemd/resolved.conf`
* Add `DNSStubListener=no`
* `sudo systemctl daemon-reload`
* `sudo systemctl restart systemd-resolved.service`
* `sudo systemctl disable systemd-resolved`
* `sudo systemctl stop systemd-resolved`
* `rm /etc/resolv.conf`

## Old info
```
ansible_become_password: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          38636365346337376330613638336666666330366331333334633065356262613464313564643163
          6435663135636132343661633465366465373964303363630a353039373633616637323165333131
          62636434623739303963623761643563656338363238643263393763653965326335633735616464
          6462333535623363610a633030333231626433383463663338666130626636396135323566356433
          6337
```

### Encrypt new string
* `ansible-vault encrypt_string --ask-vault-pass '{password}' --name 'ansible_become_password'`