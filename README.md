# 1505 Rochester Network 

## Network Diagram

```mermaid
graph LR;
    Y[olearycrew.mynetgear.com]-->A
    A[Verizon FIOS box]-->B>Verizon Router]
    B-->D>Orbi Router 10.0.0.1]
    E>Orbi Satellite 10.0.0.2]
    
    subgraph FIOS Network 192.168.1.0/24
    B-->C((Batcave Wireless))
    end
    
    subgraph Main Network 10.0.0.0/24
    D---|Wireless backhaul|E
    D-->W((Wireless))
    W-->F((Fortress of Solitude Wireless))
    F-->K["Roku 3 (10.0.0.100)"]
    W-->J((Justice League Wireless))
    J-->guest["Wireless Guests"]
    end 
    
    subgraph Wired Network
    D-->M[Wired from Orbi Router]
    M-->O[Office Plug]
    M-->G[Kevo Bridge]
    M-->H[Smart Things Hub]
    M-->I["PiHole (10.0.0.31)"]
    M-->S["Office Switch"]
    S-->R["PiHole2 (10.0.0.21)"]
    S-->r0["pirack-0 (10.0.0.43)"]
    S-->r1["pirack-1 (10.0.0.42)"]
    S-->r2["pirack-2 (10.0.0.41)"]
    end
    
```

## Network Services
* [See Services.md](/Services.md)

## Passwords

* [Verizon Router](https://my.1password.com/vaults/7b5cjibh2aarpv2n24edpxwpbm/allitems/qktlv35mjxg2dzn3majzeq6epy)
* [Orbi Router](https://my.1password.com/vaults/7b5cjibh2aarpv2n24edpxwpbm/allitems/g6l2vwtpvl22cgm7d7n7vgubpu)
