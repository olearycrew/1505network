## Diagram
`See below diagram for more details on each service`

```mermaid
graph LR;
    subgraph Liscio Apps
    R["GitLab Runners"]
    P["Prometheus"]
    G["Grafana"]
    E["ELK Stack"]
    end
    
    subgraph O'Leary Crew
    VPN["Wireguard VPN"]
    cir["Circle from Disney"]
    end

    subgraph Orbi Router
    cir-->C["Circle"]
    end
    
    subgraph 1505-pirack-0
    rack0["IP Address: 10.0.0.43"]
    R-->R0["Runner 0"]
    VPN-->pivpn["piVPN / OpenVPN"]
    end
    
    subgraph 1505-pirack-1
    rack1["IP Address: 10.0.0.42"]
    R-->R1["Runner 1"]
    end
    
    subgraph 1505-pirack-2
    rack2["IP Address: 10.0.0.41"]
    R-->R2["Runner 2"]
    G-->graf["Grafana"]
    end
    
    subgraph Mac Mini
    mini["IP Address: 10.0.0.33"]
    prom["Prometheus (see /olearycrew/1505prometheus)"]
    elk["ELK Stack"]
    P-->prom
    E-->elk
    end
```

### Prometheus
Prometheus runs on the Mac Mini in the office.

* Hostname:   `brendan-Macmini`
* IP Address: `10.0.0.33`
* Service Endpoint: `http://10.0.0.33:9090/graph`
* Service Config:   https://gitlab.com/olearycrew/1505prometheus

### Grafana
Grafana runs on one of the Raspberry Pi devices

* Hostname:   `1505-pirack-2`
* IP Address: `10.0.0.41`
* Service Endpoint: `http://10.0.0.41:3000`

### piVPN / OpenVPN
Wiregauard runs on one of the Raspberry Pi devices.


* Hostname:   `1505-pirack-0`
* IP Address: `10.0.0.43`
* Service Endpoint: `10.0.0.43:1505`
* DNS: olearycrew.mynetgear.com

### ELK Stack
Running on the Mac Mini (10.0.0.33)

Installed with https://www.digitalocean.com/community/tutorials/how-to-install-elasticsearch-logstash-and-kibana-elastic-stack-on-ubuntu-18-04

See [ELK Stack.md](https://gitlab.com/olearycrew/1505network/blob/master/ELK%20Stack.md)
